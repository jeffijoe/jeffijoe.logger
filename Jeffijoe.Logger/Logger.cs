using System;
using System.IO;
using System.Text;

namespace Jeffijoe.Logger
{
    /// <summary>
    ///     Jeffijoe.Logger - simple lightweight logger.
    /// </summary>
    public class Logger
    {
        /// <summary>
        /// Raised when a logging occurs.
        /// </summary>
        public event LoggingEvent OnLog;

        /// <summary>
        /// Occurs when [on commit].
        /// </summary>
        public event CommitEvent OnCommit;

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="Logger"/> is enabled.
        /// </summary>
        /// <value>
        ///   <c>true</c> if enabled; otherwise, <c>false</c>.
        /// </value>
        public bool Enabled { get; set; }

        /// <summary>
        /// The _commits
        /// </summary>
        public StringBuilder Commits { get; set; }

        /// <summary>
        ///     The File Path
        /// </summary>
        public string FilePath { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [include timestap].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [include timestap]; otherwise, <c>false</c>.
        /// </value>
        public bool IncludeTimestap { get; set; }

        /// <summary>
        /// Gets or sets the timestamp format.
        /// </summary>
        /// <value>
        /// The timestamp format.
        /// </value>
        public string TimestampFormat { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether [log exceptions].
        /// </summary>
        /// <value>
        ///   <c>true</c> if [log exceptions]; otherwise, <c>false</c>.
        /// </value>
        public bool LogExceptionLineInfo { get; set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logger"/> class.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="includeTimestap">if set to <c>true</c> [include timestap].</param>
        /// <param name="logExceptionLineInfo">if set to <c>true</c> [log exception line info].</param>
        public Logger(string filePath, bool includeTimestap, bool logExceptionLineInfo) : this(filePath,includeTimestap)
        {
            // Should we log exception information? (Line, number)
            LogExceptionLineInfo = logExceptionLineInfo;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logger"/> class.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="includeTimestap">if set to <c>true</c> [include timestap].</param>
        public Logger(string filePath, bool includeTimestap) : this(filePath)
        {
            // Set value indicating if we should include a timestamp when logging.
            IncludeTimestap = includeTimestap;
        }

        /// <summary>
        ///     Initializes a new instance of the <see cref="Logger" /> class.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        public Logger(string filePath) : this()
        {
            // Set filepath
            FilePath = filePath;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Logger"/> class.
        /// </summary>
        public Logger()
        {
            // Enable by default
            Enabled = true;

            // Create the stringbuilder.
            Commits = new StringBuilder();

            // Set default timestamp format.
            TimestampFormat = "dd-MM-yyyy hh:mm:ss";
        }

        /// <summary>
        /// Logs the input to the log file.
        /// </summary>
        public virtual string Log(string input)
        {
            // Return if disabled
            if (!Enabled)
                return string.Empty;

            // Construct Event args
            var args = ConstructLogEventArgs(input);
            var message = args.FormattedMessage;

            // Write to log file.
            using (var sw = File.AppendText(FilePath))
            {
                sw.WriteLine(message);
                OnLog(args);
            }

            // Return message
            return message;
        }

        /// <summary>
        /// Logs the specified strings.
        /// </summary>
        /// <param name="strings">The strings.</param>
        public virtual string Log(StringBuilder strings)
        {
            // Return if disabled
            if (!Enabled)
                return string.Empty;

            // Construct Event Args
            var args = ConstructLogEventArgs(strings.ToString());
            var message = args.FormattedMessage;

            // Write to file
            using (var sw = File.AppendText(FilePath))
            {
                sw.WriteLine(message);
            }

            // Raise event, return message
            OnLog(args);
            return message;
        }

        /// <summary>
        /// Logs the specified ex.
        /// </summary>
        /// <param name="ex">The exceptio.</param>
        /// <param name="prependMessage">The prepend message.</param>
        public virtual string Log(Exception ex, string prependMessage = "")
        {
            // Return if disabled
            if (!Enabled)
                return string.Empty;

            // Construct Event Args.
            var args = ConstructLogEventArgs(prependMessage, ex);
            args.FormattedMessage += ex.Message;
            args.FormattedMessage += Environment.NewLine;
            args.FormattedMessage += "- Exception source: " + ex.GetFileNameAndLineNumber(0);

            // Write to file
            using (var sw = File.AppendText(FilePath))
            {
                sw.WriteLine(args.FormattedMessage);
            }

            // Raise event args and return message.
            OnLog(args);
            return args.FormattedMessage;
        }

        /// <summary>
        /// Adds the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        public virtual string Add(string input)
        {
            // Prepares the input, and adds it to the Commits.
            var message = PrepareLine(input);
            Commits.AppendLine(message);
            return message;
        }

        /// <summary>
        /// Commits to the log.
        /// </summary>
        public virtual string Commit()
        {
            // Return if disabled
            if (!Enabled)
                return string.Empty;

            // Get Commits as string (trim newlines)
            var commits = Commits.ToString().Trim('\r', '\n');

            // Write them to log file
            using (var sw = File.AppendText(FilePath))
            {
                sw.Write(commits);
            }

            // Call Commit event, clear the commits, and return.
            OnCommit(commits);
            Commits.Clear();
            return commits;
        }

        /// <summary>
        /// Prepares the line - includes timestamp info if specified.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        private string PrepareLine(string input)
        {
            // Assign result to empty string
            var result = string.Empty;

            // If we should include a timestamp, do so.
            if(IncludeTimestap)
            {
                result = string.Format("[{0}] ", DateTime.Now.ToString(TimestampFormat));
            }

            // Add input and return
            result += input;
            return result;
        }

        /// <summary>
        /// Constructs the event args.
        /// </summary>
        /// <param name="message">The message.</param>
        /// <param name="ex">The ex.</param>
        /// <returns></returns>
        private LoggingEventArgs ConstructLogEventArgs(string message, Exception ex = null)
        {
            // Construct a new Logging Event Args and return it.
            var args = new LoggingEventArgs
                {
                    OriginalMessage = message,
                    FormattedMessage = PrepareLine(message),
                    Exception = ex,
                    Timestamp = DateTime.Now
                };
            return args;
        }
    }
}