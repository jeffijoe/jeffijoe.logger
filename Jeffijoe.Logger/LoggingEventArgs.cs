﻿using System;

namespace Jeffijoe.Logger
{
    /// <summary>
    /// Logging Event Arguments.
    /// </summary>
    public class LoggingEventArgs
    {
        /// <summary>
        /// Gets or sets the original message.
        /// </summary>
        /// <value>
        /// The original message.
        /// </value>
        public string OriginalMessage { get; set; }

        /// <summary>
        /// Gets or sets the full message.
        /// </summary>
        /// <value>
        /// The full message.
        /// </value>
        public string FormattedMessage { get; set; }

        /// <summary>
        /// Gets or sets the timestamp.
        /// </summary>
        /// <value>
        /// The timestamp.
        /// </value>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// Gets or sets the exception.
        /// </summary>
        /// <value>
        /// The exception.
        /// </value>
        public Exception Exception { get; set; }
    }
}