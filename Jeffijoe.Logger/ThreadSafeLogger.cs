using System;
using System.Text;

namespace Jeffijoe.Logger
{
    public class ThreadSafeLogger : Logger
    {
        private readonly object _lock = new object();

        /// <summary>
        /// Initializes a new instance of the <see cref="ThreadSafeLogger"/> class.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="includeTimestap">if set to <c>true</c> [include timestap].</param>
        /// <param name="logExceptionLineInfo">if set to <c>true</c> [log exception line info].</param>
        public ThreadSafeLogger(string filePath, bool includeTimestap, bool logExceptionLineInfo)
            : base(filePath, includeTimestap, logExceptionLineInfo)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ThreadSafeLogger"/> class.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="includeTimestap">if set to <c>true</c> [include timestap].</param>
        public ThreadSafeLogger(string filePath, bool includeTimestap) : base(filePath, includeTimestap)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ThreadSafeLogger"/> class.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        public ThreadSafeLogger(string filePath) : base(filePath)
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ThreadSafeLogger"/> class.
        /// </summary>
        public ThreadSafeLogger()
        {
        }

        /// <summary>
        /// Logs the input to the log file.
        /// </summary>
        public override string Log(string input)
        {
            lock (_lock)
            {
                return base.Log(input);
            }
        }

        /// <summary>
        /// Logs the specified strings.
        /// </summary>
        /// <param name="strings">The strings.</param>
        public override string Log(StringBuilder strings)
        {
            lock (_lock)
            {
                return base.Log(strings);
            }
        }

        /// <summary>
        /// Logs the specified ex.
        /// </summary>
        /// <param name="ex">The exceptio.</param>
        /// <param name="prependMessage">The prepend message.</param>
        public override string Log(Exception ex, string prependMessage = "")
        {
            lock (_lock)
            {
                return base.Log(ex, prependMessage);
            }
        }

        /// <summary>
        /// Adds the specified input.
        /// </summary>
        /// <param name="input">The input.</param>
        public override string Add(string input)
        {
            lock (_lock)
            {
                return base.Add(input);
            }
        }

        /// <summary>
        /// Commits to the log.
        /// </summary>
        public override string Commit()
        {
            lock (_lock)
            {
                return base.Commit();
            }
        }
    }
}