﻿using System;
using System.Diagnostics;

namespace Jeffijoe.Logger
{
    /// <summary>
    /// Exception Helper
    /// </summary>
    public static class ExceptionHelper
    {
        /// <summary>
        /// Gets the line number that thew the exception
        /// </summary>
        /// <param name="ex"></param>
        /// <param name="stackFrame">Stack frame to get line from</param>
        /// <returns></returns>
        public static string GetFileNameAndLineNumber(this Exception ex,int stackFrame)
        {
            // Get stack trace for the exception with source file information
            var st = new StackTrace(ex, true);
            if (st.FrameCount <= stackFrame)
                return "[No such stack frame]";
            // Get the top stack frame
            var frame = st.GetFrame(stackFrame);
            // Get the line number from the stack frame
            var line = frame.GetFileLineNumber();
            var fileName = frame.GetFileName();
            return fileName + " -> L" + line.ToString();
        }
    }
}
