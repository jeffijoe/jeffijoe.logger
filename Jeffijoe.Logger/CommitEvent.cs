namespace Jeffijoe.Logger
{
    /// <summary>
    /// Commit Event Arguments, for the OnCommit Event.
    /// </summary>
    public delegate void CommitEvent(string commits);
}