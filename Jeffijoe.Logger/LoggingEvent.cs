﻿namespace Jeffijoe.Logger
{
    /// <summary>
    /// Logging Event
    /// </summary>
    /// <param name="args">The <see cref="LoggingEventArgs"/> instance containing the event data.</param>
    public delegate void LoggingEvent(LoggingEventArgs args);
}